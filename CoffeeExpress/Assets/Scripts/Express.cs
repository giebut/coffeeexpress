﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Express : MonoBehaviour {
    public delegate void Void(bool value);
    public static event Void activateEvent;

    enum Status {
        Busy,
        Ready,
        Waiting
    }

    public Text statusDisplay;
    public Image progressDisplay;
    public Selectable activateToggle;
    public Selectable cupButton;
    public Selectable makeButton;
    public Toggle ratioToggleTemplate;
    public Toggle volumeToggleTemplate;

    public Container waterContainer;
    public Container beansContainer;
    public Container groundsContainer;
    public Tray tray;

    [HideInInspector]
    public float brewingDuration;
    [HideInInspector]
    public RatioTable ratioTable;
    [HideInInspector]
    public VolumeTable volumeTable;

    int ratioIndex;
    int volumeIndex;
    Coroutine makeCoroutine;
    Coroutine activateCoroutine;
    Status status;

    [SerializeField, HideInInspector]
    ContainerMatrix containerMatrix;

    void Awake() {
        statusDisplay.text = "";

        ratioIndex = PlayerPrefs.GetInt(Name.ratioIndex, 0);

        for(int i = 1; i < ratioTable.array.Length; i++) {
            var clone = Instantiate(ratioToggleTemplate, ratioToggleTemplate.transform.parent);
            clone.isOn = ratioIndex == i;
            clone.GetComponentInChildren<Text>().text = ratioTable.array[i].type;

            int index = i;

            clone.onValueChanged.AddListener(value => {
                if(value == true) {
                    setRatioIndex(index);
                }
            });
        }

        ratioToggleTemplate.isOn = ratioIndex == 0;
        ratioToggleTemplate.GetComponentInChildren<Text>().text = ratioTable.array[0].type;

        ratioToggleTemplate.onValueChanged.AddListener(value => {
            if(value == true) {
                setRatioIndex(0);
            }
        });

        for(int i = 1; i < volumeTable.array.Length; i++) {
            var clone = Instantiate(volumeToggleTemplate, volumeToggleTemplate.transform.parent);
            clone.isOn = volumeIndex == i;
            clone.GetComponentInChildren<Text>().text = volumeTable.array[i].type;

            int index = i;

            clone.onValueChanged.AddListener(value => {
                if(value == true) {
                    setVolumeIndex(index);
                }
            });
        }

        volumeToggleTemplate.isOn = volumeIndex == 0;
        volumeToggleTemplate.GetComponentInChildren<Text>().text = volumeTable.array[0].type;

        volumeToggleTemplate.onValueChanged.AddListener(value => {
            if(value == true) {
                setVolumeIndex(0);
            }
        });
    }

    void Start() {
        waterContainer.initFull();
        beansContainer.initFull();
        groundsContainer.initEmpty();

        status = Status.Waiting;
    }

    void OnValidate() {
        PlayerPrefs.DeleteAll();
    }

    void setRatioIndex(int value) {
        ratioIndex = value;

        PlayerPrefs.SetInt(Name.ratioIndex, ratioIndex);
    }

    void setVolumeIndex(int value) {
        volumeIndex = value;

        PlayerPrefs.SetInt(Name.volumeIndex, volumeIndex);
    }

    public void take() {
        if(status == Status.Ready) {
            activateToggle.interactable = true;
            cupButton.interactable = false;
            makeButton.interactable = true;
            status = Status.Waiting;
            statusDisplay.text = status.ToString();
        }
    }

    public void make() {
        float requiredBeansVolume = ratioTable.array[ratioIndex].value * volumeTable.array[volumeIndex].value;
        float requiredWaterVolume = (1f - ratioTable.array[ratioIndex].value) * volumeTable.array[volumeIndex].value;

        if(status == Status.Waiting) {
            if(waterContainer.checkStatus(requiredWaterVolume)) {
                statusDisplay.text = waterContainer.getStatusString();
            } else if(beansContainer.checkStatus(requiredBeansVolume)) {
                statusDisplay.text = beansContainer.getStatusString();
            } else if(groundsContainer.isFull) {
                statusDisplay.text = groundsContainer.getStatusString();
            } else if(tray.isFull) {
                statusDisplay.text = tray.name + Tray.fullStatus;
            } else {
                makeCoroutine = StartCoroutine(makeCountdown(requiredBeansVolume, requiredWaterVolume));
            }
        }
    }

    IEnumerator makeCountdown(float requiredBeansVolume, float requiredWaterVolume) {
        float beansVolume = 0f;

        makeButton.interactable = false;
        activateToggle.interactable = false;
        status = Status.Busy;
        statusDisplay.text = status.ToString() + ": Beans";

        while(beansVolume < requiredBeansVolume) {
            beansVolume += beansContainer.pour();
            progressDisplay.fillAmount = beansVolume / requiredBeansVolume;

            yield return null;
        }

        float waterVolume = 0f;

        statusDisplay.text = status.ToString() + ": Water";

        while(waterVolume < requiredWaterVolume) {
            waterVolume += waterContainer.pour();
            progressDisplay.fillAmount = waterVolume / requiredWaterVolume;

            yield return null;
        }

        float time = 0f;

        statusDisplay.text = status.ToString() + ": Brewing";

        while(time < brewingDuration) {
            time = Mathf.MoveTowards(time, brewingDuration, Time.deltaTime);
            progressDisplay.fillAmount = time / brewingDuration;

            yield return null;
        }

        float groundsVolume = 0;
        cupButton.interactable = true;
        status = Status.Ready;
        statusDisplay.text = status.ToString() + ": Grounds";

        while(groundsVolume < beansVolume) {
            if(!groundsContainer.isFull) {
                beansVolume -= groundsContainer.fill();
                progressDisplay.fillAmount = groundsVolume / beansVolume;
            } else {
                statusDisplay.text = groundsContainer.getStatusString();
            }

            yield return null;
        }

        tray.fill(waterVolume - requiredWaterVolume);

        statusDisplay.text = status.ToString();
    }

    public void activate(bool value) {
        if(activateEvent != null) {
            activateEvent(value);
        }

        if(value) {
            activateCoroutine = StartCoroutine(activateCountdown());
        } else {
            waterContainer.save();
            beansContainer.save();
            groundsContainer.save();
            tray.save();
        }
    }

    IEnumerator activateCountdown() {
        status = Status.Waiting;
        statusDisplay.text = status.ToString();

        while(waterContainer.isEmpty) {
            statusDisplay.text = waterContainer.getStatusString();
            progressDisplay.fillAmount = waterContainer.volumeRatio;

            yield return null;
        }

        while(beansContainer.isEmpty) {
            statusDisplay.text = beansContainer.getStatusString();
            progressDisplay.fillAmount = beansContainer.volumeRatio;

            yield return null;
        }

        while(groundsContainer.isFull) {
            statusDisplay.text = groundsContainer.getStatusString();
            progressDisplay.fillAmount = groundsContainer.volumeRatio;

            yield return null;
        }

        statusDisplay.text = status.ToString();
    }

    public void setContainerMatrix() {
        containerMatrix.waterContainer = waterContainer;
        containerMatrix.beansContainer = beansContainer;
        containerMatrix.groundsContainer = groundsContainer;
    }
}