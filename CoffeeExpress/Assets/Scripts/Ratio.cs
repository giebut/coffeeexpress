﻿using UnityEngine;

[System.Serializable]
public struct Ratio {
    public string type;
    [Range(0f, 1f)]
    public float value;
}
