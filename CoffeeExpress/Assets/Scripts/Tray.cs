﻿using UnityEngine;
using UnityEngine.UI;

public class Tray : MonoBehaviour {
    public const string fullStatus = "is full";

    public float maxVolume;
    public Image indicator;

    float volume;
    Coroutine emptyCoroutine;

    public bool isFull { get { return volume >= maxVolume; } }

    void Awake() {
        volume = PlayerPrefs.GetFloat(name, 0f);
        indicator.fillAmount = volume / maxVolume;
    }

    void OnDestroy() {
        save();
    }

    public void save() {
        PlayerPrefs.SetFloat(name, volume);
    }

    public void empty() {
        volume = 0f;
        indicator.fillAmount = volume / maxVolume;
    }

    public void fill(float value) {
        volume += value;
        indicator.fillAmount = volume / maxVolume;
    }
}
