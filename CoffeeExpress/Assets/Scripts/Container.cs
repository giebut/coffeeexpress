﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Container : MonoBehaviour {
    enum Status {
        Empty,
        Low,
        Enough,
        Full,
        Empting,
        Filling
    }

    public bool isInverted;
    public float maxVolume;
    public float fillSpeed;
    public float pourSpeed;
    public Image indicator;

    float volume;
    Color emptyColor;
    Color fullColor;
    Coroutine emptyCoroutine;
    Coroutine fulfillCoroutine;
    Status status;

    public bool isEmpty { get { return status == Status.Empty; } }
    public bool isFull { get { return status == Status.Full; } }
    public float volumeRatio { get { return volume / maxVolume; } }

    void Awake() {
        if(isInverted) {
            emptyColor = Color.green;
            fullColor = Color.red;
        } else {
            emptyColor = Color.red;
            fullColor = Color.green;
        }
    }

    void OnDestroy() {
        save();
    }

    void OnValidate() {
        PlayerPrefs.DeleteAll();
    }

    public void initFull() {
        volume = PlayerPrefs.GetFloat(name + Name.propertyMaxVolume, maxVolume);

        init();
    }

    public void initEmpty() {
        volume = PlayerPrefs.GetFloat(name + Name.propertyMaxVolume, 0f);

        init();
    }

    void init() {
        if(volume == maxVolume) {
            status = Status.Full;
            indicator.color = fullColor;
            indicator.fillAmount = 1f;
        } else if(volume == 0f) {
            status = Status.Empty;
            indicator.color = emptyColor;
            indicator.fillAmount = 1f;
        } else {
            status = Status.Empty;
            indicator.fillAmount = volume / maxVolume;
            indicator.color = Color.Lerp(emptyColor, fullColor, indicator.fillAmount);
        }
    }

    public void save() {
        PlayerPrefs.SetFloat(name + Name.propertyMaxVolume, volume);
    }

    public string getStatusString() {
        return name + " is " + status.ToString().ToLower();
    }

    public void empty() {
        emptyCoroutine = StartCoroutine(emptyCountdown());
    }

    IEnumerator emptyCountdown() {
        status = Status.Empting;

        while(volume > 0f) {
            volume = Mathf.MoveTowards(volume, 0, pourSpeed * Time.deltaTime);

            indicator.fillAmount = volume / maxVolume;
            indicator.color = Color.Lerp(emptyColor, fullColor, indicator.fillAmount);

            yield return null;
        }

        status = Status.Empty;

        indicator.color = emptyColor;
        indicator.fillAmount = 1f;
    }

    public void fulfill() {
        fulfillCoroutine = StartCoroutine(fulfillCountdown());
    }

    IEnumerator fulfillCountdown() {
        status = Status.Filling;

        while(volume < maxVolume) {
            volume = Mathf.MoveTowards(volume, maxVolume, fillSpeed * Time.deltaTime);

            indicator.fillAmount = volume / maxVolume;
            indicator.color = Color.Lerp(emptyColor, fullColor, indicator.fillAmount);

            yield return null;
        }

        status = Status.Full;
    }

    public float fill() {
        float amount = fillSpeed * Time.deltaTime;

        if(amount > maxVolume - volume) {
            amount = maxVolume - volume;
        }

        volume += amount;

        indicator.fillAmount = volume / maxVolume;
        indicator.color = Color.Lerp(emptyColor, fullColor, indicator.fillAmount);

        return amount;
    }

    public float pour() {
        float amount = pourSpeed * Time.deltaTime;

        if(amount > volume) {
            amount = volume;
        }

        volume -= amount;

        if(volume > 0f) {
            indicator.fillAmount = volume / maxVolume;
            indicator.color = Color.Lerp(emptyColor, fullColor, indicator.fillAmount);
        } else {
            indicator.color = emptyColor;
            indicator.fillAmount = 1f;
        }

        return amount;
    }

    public bool checkStatus(float value) {
        if(isInverted) {
            if(volume > maxVolume - value) {
                status = volume < maxVolume ? Status.Low : Status.Full ;

                return true;
            } else {
                status = volume > 0f ? Status.Enough : Status.Empty;

                return false;
            }
        } else {
            if(volume < value) {
                status = volume > 0f ? Status.Low : Status.Empty;

                return true;
            } else {
                status = volume < maxVolume ? Status.Enough : Status.Full;

                return false;
            }
        }
    }
}
