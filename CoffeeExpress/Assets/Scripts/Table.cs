﻿[System.Serializable]
public class Table<Type>{
    public Type[] array;
}

[System.Serializable]
public class RatioTable : Table<Ratio> { }

[System.Serializable]
public class VolumeTable : Table<Volume> { }