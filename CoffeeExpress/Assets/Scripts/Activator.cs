﻿using UnityEngine;
using UnityEngine.UI;

public class Activator : MonoBehaviour {
    Selectable selectable;

    void Awake() {
        selectable = GetComponent<Selectable>();

        Express.activateEvent += activate;
    }

    void OnDestroy() {
        Express.activateEvent -= activate;
    }

    void activate(bool value) {
        if(selectable != null) {
            selectable.interactable = value;
        }
    }
}
