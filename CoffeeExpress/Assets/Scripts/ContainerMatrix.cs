﻿[System.Serializable]
public struct ContainerMatrix {
    public Container waterContainer;
    public Container beansContainer;
    public Container groundsContainer;
}
