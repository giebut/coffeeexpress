﻿using System;
using UnityEditor;

[CustomPropertyDrawer(typeof(RatioTable))]
public class RatioTablePropertyDrawer : TablePropertyDrawer {
    protected override string title { get { return Name.ratioTitle; } }
    protected override string firstHeader { get { return Name.ratioFirstHeader; } }
    protected override string secondHeader { get { return Name.ratioSecondHeader; } }
}