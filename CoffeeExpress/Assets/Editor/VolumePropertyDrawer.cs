﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Volume))]
public class VolumePropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        var name = property.FindPropertyRelative(Name.propertyType);
        var nameRect = new Rect(position.x, position.y, 0.3f*position.width, position.height);
        EditorGUI.PropertyField(nameRect, name, GUIContent.none);

        var value = property.FindPropertyRelative(Name.propertyValue);
        var valueRect = new Rect(position.x + 0.3f * position.width, position.y, 0.7f * position.width, position.height);
        EditorGUI.PropertyField(valueRect, value, GUIContent.none);
    }
}
