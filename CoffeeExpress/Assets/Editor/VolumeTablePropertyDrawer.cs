﻿using UnityEditor;

[CustomPropertyDrawer(typeof(VolumeTable))]
public class VolumeTablePropertyDrawer : TablePropertyDrawer {
    protected override string title { get { return Name.volumeTitle; } }
    protected override string firstHeader { get { return Name.volumeFirstHeader; } }
    protected override string secondHeader { get { return Name.volumeSecondHeader; } }
}