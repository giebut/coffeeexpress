﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ContainerMatrix))]
public class ContainerMatrixPropertyDrawer : PropertyDrawer {
    static readonly GUIContent labelMaxVolume = new GUIContent("Max volume");
    static readonly GUIContent labelFillSpeed = new GUIContent("Fill speed");
    static readonly GUIContent labelPourSpeed = new GUIContent("Pour speed");

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        var rect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        float columnWidth = 0.25f * rect.width;

        EditorGUI.LabelField(new Rect(rect.x + columnWidth, rect.y, columnWidth, rect.height), labelMaxVolume);
        EditorGUI.LabelField(new Rect(rect.x + 2f * columnWidth, rect.y, columnWidth, rect.height), labelFillSpeed);
        EditorGUI.LabelField(new Rect(rect.x + 3f * columnWidth, rect.y, columnWidth, rect.height), labelPourSpeed);

        rect.y += EditorGUIUtility.singleLineHeight;
        drawContainer(rect, property, columnWidth, Name.propertyWaterContainer, Name.titleWater);

        rect.y += EditorGUIUtility.singleLineHeight;
        drawContainer(rect, property, columnWidth, Name.propertyBeansContainer, Name.titleBeans);

        rect.y += EditorGUIUtility.singleLineHeight;
        drawContainer(rect, property, columnWidth, Name.propertyGroundsContainer, Name.titleGrounds);

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return 4f * EditorGUIUtility.singleLineHeight;
    }

    void drawContainer(Rect rect, SerializedProperty property, float columnWidth, string containerName, string title) {
        EditorGUI.LabelField(new Rect(rect.x, rect.y, columnWidth, rect.height), new GUIContent(title), EditorStyles.miniBoldLabel);

        var containerProperty = property.FindPropertyRelative(containerName);
        var containerSerializedObject = new SerializedObject(containerProperty.objectReferenceValue);

        var maxVolume = containerSerializedObject.FindProperty(Name.propertyMaxVolume);
        EditorGUI.PropertyField(new Rect(rect.x + columnWidth, rect.y, columnWidth, rect.height), maxVolume, GUIContent.none);

        var fillSpeed = containerSerializedObject.FindProperty(Name.propertyFillSpeed);
        EditorGUI.PropertyField(new Rect(rect.x + 2f * columnWidth, rect.y, columnWidth, rect.height), fillSpeed, GUIContent.none);

        var pourSpeed = containerSerializedObject.FindProperty(Name.propertyPourSpeed);
        EditorGUI.PropertyField(new Rect(rect.x + 3f * columnWidth, rect.y, columnWidth, rect.height), pourSpeed, GUIContent.none);

        containerSerializedObject.ApplyModifiedProperties();
    }
}