﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class ExpressConfiguration : EditorWindow {
    public static ExpressConfiguration instance;

    static Express Express;

    static ExpressConfiguration() {
        Express = null;

        EditorApplication.update += Update;
    }

    static void Update() {
        var activeExpress = Selection.activeGameObject == null ? null : Selection.activeGameObject.GetComponent<Express>();

        if(activeExpress == null) {
            Express = null;

            CloseWindow();
        } else {
            if(Express != activeExpress) {
                Express = activeExpress;

                Express.setContainerMatrix();

                ShowWindow(Express);
            }
        }
    }

    static void ShowWindow(Express express) {
        if(instance == null) {
            instance = GetWindow(typeof(ExpressConfiguration)) as ExpressConfiguration;

            instance.express = express;
        }
    }

    static void CloseWindow() {
        if(instance != null) {
            instance.Close();
        }
    }

    Express express;
    Rect ratiosHeaderRect;
    Rect volumesHeaderRect;

    void OnGUI() {
        if(express != null) {
            var serializedObject = new SerializedObject(express);

            GUILayout.Label(Name.titleContainers, EditorStyles.boldLabel);

            var containerMatrix = serializedObject.FindProperty(Name.propertyContainerMatrix);
            EditorGUILayout.PropertyField(containerMatrix);

            GUILayout.Space(EditorGUIUtility.singleLineHeight);

            if(express.tray != null) {
                express.tray.maxVolume = EditorGUILayout.FloatField("Tray max volume", express.tray.maxVolume);
            }

            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            GUILayout.Label(Name.titleCoffeeSettings, EditorStyles.boldLabel);

            var brewingDuration = serializedObject.FindProperty(Name.propertBrewingDuration);
            brewingDuration.floatValue = EditorGUILayout.FloatField(Name.titleBrewingDuration, brewingDuration.floatValue);

            var ratioTable = serializedObject.FindProperty(Name.propertyRatioTable);
            EditorGUILayout.PropertyField(ratioTable);

            var volumeTable = serializedObject.FindProperty(Name.propertyVolumeTable);
            EditorGUILayout.PropertyField(volumeTable);

            serializedObject.ApplyModifiedProperties();
        }
    }

    void drawContainer(string title, Container container) {
        GUILayout.Label(title, EditorStyles.miniBoldLabel);

        container.maxVolume = EditorGUILayout.FloatField(Name.titleMaxVolume, container.maxVolume);
        container.fillSpeed = EditorGUILayout.FloatField(Name.titleFillSpeed, container.fillSpeed);
        container.pourSpeed = EditorGUILayout.FloatField(Name.titlePourSpeed, container.pourSpeed);
    }
}
