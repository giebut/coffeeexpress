﻿using UnityEditor;
using UnityEngine;

public abstract class TablePropertyDrawer : PropertyDrawer {
    protected abstract string title {get;}
    protected abstract string firstHeader {get;}
    protected abstract string secondHeader {get;}

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        var array = property.FindPropertyRelative(Name.propertyArray);
        var rect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

        EditorGUI.LabelField(rect, title, EditorStyles.miniBoldLabel);
        rect.y += EditorGUIUtility.singleLineHeight;
        array.arraySize = EditorGUI.IntField(rect, Name.propertySize, array.arraySize);
        rect.y += EditorGUIUtility.singleLineHeight;

        EditorGUI.LabelField(
            new Rect(rect.x, rect.y, 0.3f * rect.width, rect.height),
            new GUIContent(firstHeader)
            );
        EditorGUI.LabelField(
            new Rect(rect.x + 0.3f * rect.width, rect.y, 0.7f * rect.width, rect.height),
            new GUIContent(secondHeader)
            );

        for(int i = 0; i < array.arraySize; i++) {
            rect.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(rect, array.GetArrayElementAtIndex(i));
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        var array = property.FindPropertyRelative(Name.propertyArray);

        return EditorGUIUtility.singleLineHeight * (array.arraySize + 3);
    }
}